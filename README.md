# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Introduction:

This project is related to Indian show “Kaun Banega Crorepati” in which host ask some question and other person give answer. But in this project we already have some questions in file that will load up during when program runs.
Purpose:

Purpose of this project is to give you an idea how binary file handling works in c++. How you can randomly generate question from file and how to read from file.

### How do I get set up? ###

Features:

Binary file handling.

Using of struct in c++

Reading from file

Generating question from file

Usage:

Run the project

Press enter and timer will start

Question and their multiple answers will be shown up

You can use lifeline

Score will be shown to the top right side of screen.

If you give wrong answer then game will end.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
Blog : http://programminglearner143.blogspot.com/2017/09/kbc-introduction-this-project-isrelated.html

Email: waqashaxhmi143@outlook.com